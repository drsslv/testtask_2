﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SalovConcolApp
{
    class Program
    {
       
        public static void Shuffle(Random rng, int[] array )
        {
            int n = array.Length;
            while (n > 1)
            {
                int k = rng.Next(n--);
                var temp = array[n];
                array[n] = array[k];
                array[k] = temp;
            }
        }
        static void Main(string[] args)
        {
            // shuffled array generation
            const int N = 21;
            var rand = new Random();
            int[] mas = new int[N];
            for (int i = 0; i < N; i++)
            {
                mas[i] = i;
               
            }
            Shuffle(rand, mas);
            
            // write array to file
            using (var sw = new StreamWriter("file.txt")) // SalovConcolApp\bin\Debug\file.txt
            {
                
                for (int i = 0; i < N; i++)
                {
                    if (i == mas.Length - 1) sw.Write(mas[i]);
                    else sw.Write("{0},", mas[i]);
                }
            }
            // read from file
            using (var sr = new StreamReader("file.txt"))
            {
                var text = sr.ReadToEnd();
                Console.WriteLine("Из файла прочитано ");
                foreach (var i in text) Console.Write(i);
                
                // get numbers array without ,
                var array = text.Split(',').Select(x => int.Parse(x)).ToArray();
                // ascending array
                var ask =array.OrderBy(x => x).ToArray();
                // descending array
                var desk = array.OrderByDescending(x => x).ToArray();
                Console.WriteLine("\nРезультат сортировки по возрастанию");
                for (int i = 0; i < N; i++)
                {
                    if (i == ask.Length - 1) Console.Write(ask[i]);
                    else Console.Write("{0},", ask[i]);
                }
                Console.WriteLine("\nРезультат сортировки по убыванию");
                for (int i = 0; i < N;i++)
                {
                    if (i == desk.Length - 1) Console.Write(desk[i]);
                    else Console.Write("{0},", desk[i]);
                }
               





            }
                Console.ReadLine();
        }
    }
}
